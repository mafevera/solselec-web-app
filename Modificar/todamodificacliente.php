<html lang="es">
<head>
<title>Solselec:Soluciones en seguridad electronica</title>
     <meta charset="utf-8"/>

<link rel="STYLESHEET" type="text/css" href="../CSS/estilomenu1.css">	
<link rel="STYLESHEET" type="text/css" href="../CSS/estilo.css">	 

 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no, maximun-scale=1.0, minimum-scale=1.0">
 

</head>
<body style="background-image:url(../Imagenes/13.jpg);
	background-repeat:repeat;
	background-size:400px 200px;
	width:100%;">

<?php
include("menucliente.php");
menu();
?>


<div class="social">
		<ul>
			<li><a href="https://es-la.facebook.com/" target="_blank" class="facebook"><img src="../Imagenes/fac.png" ></a></li>
			<li><a href="http://www.twitter.com/" target="_blank" class="twitter"><img src="../Imagenes/tw.png" ></a></li>
			<li><a href="https://www.instagram.com/?hl=es-la" target="_blank" class="instagram"><img src="../Imagenes/instagram.png" ></a></li>
			<li><a href="https://www.youtube.com/?gl=CO&hl=es-419" target="_blank" class="youtube"><img src="../Imagenes/youtube.png" ></a></li>
			<li><a href="https://web.whatsapp.com/%F0%9F%8C%90/es"  target="_blank" class="ws"><img src="../Imagenes/ws.png" ></a></li>
		</ul>
	</div>

<br/>
<div class="noso">
<div class="vision">
<br/><div class="titulo">Modificar</div><br/>
<ul class="vision">


<li><a href="modificarcliente.php" style="text-decoration:none; color:black"><span class="icon-checkmark"></span> Modifica tus datos personales. <span class="icon-link" style=" font-size:80%; vertical-align:middle;"></span></a></li><br/>
<li><img src='../Imagenes/CCTV.jpg' style="width:100%; height:auto;"></li>
<li><a href="modificacalificacion.php" style="text-decoration:none; color:black"><span class="icon-checkmark"></span> Modifica tu calificacion a los servicios o productos de solselec <span class="icon-link" style=" font-size:80%; vertical-align:middle;"></span></a></li>
</ul>

</div>
<div class="imgus">
<img src='../Imagenes/intala.jpg'>
</div>

</br></br>
<?php
include("../piepagina.php");
pie();
?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="../JS/main.js"></script>

</body>
</html>
