

<html lang="es">
<HEAD>
    <title>LOGIN</title>
     <meta charset="utf-8"/>

<link rel="STYLESHEET" type="text/css" href="../CSS/estilomenu1.css">	
<link rel="STYLESHEET" type="text/css" href="../CSS/estilo.css">	 
<link rel="STYLESHEET" type="text/css" href="../CSS/slide.css">
	 
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no, maximun-scale=1.0, minimum-scale=1.0">
  <script type="text/javascript" src="../JS/validacion.js">
  
		
		 </script>   
</HEAD>
<body style="background-image:url(../Imagenes/13.jpg);
	background-repeat:repeat;
	background-size:400px 200px;
	width:100%;">
<?php
include("menuadministrador.php");
menu();

?>

<div class="doble">
<div class="imag">
<img src='../Imagenes/videovi.jpg'>
</div>
<div class="formu">
</br>

			<div class="titulo"> Formulario Empleado</div><br/>

<?PHP
//Primero hacemos las conexiones
 include("../conectarse.php"); 

 $conn=conectarse(); 

//hacemos la consulta de los valores que llenaran el combo 
$result = pg_query("select * from sucursal");
$result2 = pg_query("select * from fondopension");
$result3 = pg_query("select * from segurosocial");
?>  
<form method="post" action="Empleado.php">
    <center><table class="formul"><tr><td>
        <label for="Nombre">Nombre:</label></td><td>
        <input type="text" id= "Nombre" name="Nombre" placeholder="Escriba su nombre aqui"  required onkeypress="return solotexto(event)"/><br/>
		</td></tr><tr><td><label for="Apellido1">Primer Apellido:</label></td><td>
        <input type="text" name="Apellido1" placeholder="Escriba su apellido aqui"   required onkeypress="return solotexto(event)"/><br/>
		</td></tr><tr><td><label for="Apellido2">Segundo Apellido:</label></td><td>
        <input type="text" name="Apellido2" placeholder="Escriba su apellido aqui"  required onkeypress="return solotexto(event)"/><br/>
		</td></tr><tr><td><label for="cedula">Cedula de Ciudadania:</label></td><td>
        <input type="text" name="cedula" placeholder="Escriba su cedula aqui"  required onkeypress="return solonumero(event)" /><br/>
		</td></tr><tr><td><label for="rh">Tipo de Sangre:</label></td><td>
		<select name="rh"  id="micombo" required>
		<option value="" selected>-</option>
           <option value="O-">O-</option>
		   <option value="O+">O+</option>
		   <option value="A-">A-</option>
		   <option value="A+">A+</option>
		   <option value="B-">B-</option>
		   <option value="B+">B+</option>
		      <option value="AB-">AB-</option>
			     <option value="AB+">AB+</option> </select><br/>
		</td></tr><tr><td><label for="codigo_empleado">codigo:</label></td><td>
        <input type="text" name="codigo_empleado" placeholder="Escriba su codigo aqui"  required onkeypress="return solonumero(event)" /><br/>
		
		</td></tr><tr><td><label<label for="cargo">Cargo:</label></td><td>
        <input type="text" name="cargo" placeholder="Escriba su cargo aqui"  required onkeypress="return solotexto(event)"/><br/>
		</td></tr><tr><td><label for="fecha_Ingreso">Fecha de Ingreso:</label></td><td>
        <input type="date" name="fecha_Ingreso" /><br/>
		</td></tr><tr><td><label for="horario">Horario:</label></td><td>
                <select name="horario"  id="micombo" required >
				<option value="" selected  >- </option>
           <option value="8 a 12 a.m">8 a 12 a.m.</option>
		   <option value="4 a 6 p.m">4 a 6 p.m.</option>
	    </select>
		</td></tr><tr><td><label for="salario">Salario:</label></td><td>
        <input type="number" name="salario" placeholder="Escriba su salario aqui"  required onkeypress="return solonumero(event)"/><br/>
		</td></tr><tr><td><label for="fecha_nacimiento">Fecha de Nacimiento:</label></td><td>
        <input type="date" name="fecha_nacimiento" /><br/>
		</td></tr><tr><td><label for="email">email:</label></td><td>
        <input type="text" name="email" placeholder="Escriba su correo aqui"  required onchange="return validarEmail(this.value)"  /><br/>
		</td></tr><tr><td><label for="username">Usuario:</label></td><td>
        <input type="text" name="username" placeholder="Escriba su usuario aqui"  required/><br/>
		</td></tr><tr><td><label for="contra">Contraseña:</label></td><td>
        <input type="password" name="contra" placeholder="Escriba su contraseña aqui"  required/><br/>
		</td></tr><tr><td><label for="edad">Edad:</label></td><td>
        <input type="number" name="edad" placeholder="Escriba su edad aqui"  required onkeypress="return solonumero(event)"/><br/>
		 </td></tr><tr><td><label for="escolarizacion">Escolarizacion:</label></td><td>
        <input type="text" name="escolarizacion" placeholder="Escriba su nivel de estudios aqui"  required onkeypress="return solotexto(event)"/><br/>
         </td></tr><tr><td><label for="nivel_interno">Nivel Interno:</label></td><td>
        <input type="text" name="nivel_interno" placeholder="Escriba su nivel en la empresa aqui"  required onkeypress="return solotexto(event)"/><br/>
		 </td></tr><tr><td><label for="genero">genero:</label></td><td>
        <select name="genero"  id="micombo"required >
		<option value="" selected >- </option>
           <option value="femenino">Femenino</option>
		   <option value="Masculino">Masculino</option>
	    </select><br/>
        </td></tr>
		<tr><td><label for="estado">Estado:</label></td><td>
        <select name="estado"  id="micombo" required>
		<option value="" selected >- </option>
           <option value="Activo">Activo</option>
		   <option value="Inactivo">Inactivo</option>
	    </select><br/>
        </td></tr>
		<tr><td><label for="direccion">direccion:</label></td><td>
        <input type="text" name="direccion" placeholder="Escriba su direccion aqui"  required/><br/>
		  </td></tr><tr><td><label for="sucursal">Sucursal:</label></td><td>
<select name="sucursal" required> 

<?PHP
while($row = pg_fetch_array($result)) { 

$sucursal1 = $row["nombresucursal"] ;
echo "<option value=".$sucursal1.">".$sucursal1."</option>"; 

}  
?>
</select><br/>
		  </td></tr><tr><td><label for="nit_seguro">Seguro social:</label></td><td>
		  <select name="nit_seguro" required> 
		<?PHP
while($row = pg_fetch_array($result3)) { 

$nit_seguro1 = $row["nitseguro"] ;
echo "<option value=".$nit_seguro1.">".$nit_seguro1."</option>"; 

}  
?>
</select><br/>
</td></tr><tr><td><label for="nit_fondo">Fondo de Pensiones:</label></td><td>
		  <select name="nit_fondo" required> 
		<?PHP
while($row = pg_fetch_array($result2)) { 

$nit_fondo1 = $row["nitfondo"] ;
echo "<option value=".$nit_fondo1.">".$nit_fondo1."</option>"; 

}  
?>
</select><br/>
      </td></tr><tr><td><label for="telefono">telefono:</label></td><td>
        <input type="number" name="telefono" placeholder="Escriba su telefono aqui"  required onkeypress="return solonumero(event)"/><br/>
		 </td></tr><tr><td><label for="mejora">Observaciones:</label></td><td>
		<textarea name="mejora"  placeholder="Escriba aqui sus observaciones" required></textarea><br/></td></tr>
		<tr><td colspan="2"><center><input type="submit" value="Enviar"><input type="button" value="Retornar" onclick=" location.href='../menu_administrador.php' "><input type="button" value="Salir" onclick=" location.href='../salir.php' "></center></td></tr>
    </table></center>
</form>
</div>

</div>

</br></br></br></br></br>
</br></br>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="../JS/main.js"></script>
<?php
include("../piepagina.php");
pie();
?>
</body>
</html>