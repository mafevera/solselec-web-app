<!DOCTYPE html>
<html lang="es">
<head>
<title>Solselec:Soluciones en seguridad electronica</title>
     <meta charset="utf-8"/>

<link rel="STYLESHEET" type="text/css" href="CSS/estilomenu1.css">	
<link rel="STYLESHEET" type="text/css" href="CSS/estilo.css">	 

 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no, maximun-scale=1.0, minimum-scale=1.0">
 

</head>
<body style="background-image:url(Imagenes/13.jpg);
	background-repeat:repeat;
	background-size:400px 200px;
	width:100%;">

<?php
include("menuadministrador.php");
menu();
?>


<div class="social">
		<ul>
			<li><a href="https://es-la.facebook.com/" target="_blank" class="facebook"><img src="Imagenes/fac.png" ></a></li>
			<li><a href="http://www.twitter.com/" target="_blank" class="twitter"><img src="Imagenes/tw.png" ></a></li>
			<li><a href="https://www.instagram.com/?hl=es-la" target="_blank" class="instagram"><img src="Imagenes/instagram.png" ></a></li>
			<li><a href="https://www.youtube.com/?gl=CO&hl=es-419" target="_blank" class="youtube"><img src="Imagenes/youtube.png" ></a></li>
			<li><a href="https://web.whatsapp.com/%F0%9F%8C%90/es"  target="_blank" class="ws"><img src="Imagenes/ws.png" ></a></li>
		</ul>
	</div>

<br/>
<div class="noso">
<div class="vision"><br/>
<p><div class="titulo">Razon Social</div><br/> Solselec-soluciones en seguridad electronica.<br/><br/>
<div class="titulo">Mision</div><br/>garantizarles a nuestros clientes una seguridad confiable e integral con personas altamente calificadas,incorporando
la tecnologia mas innovadora y brindando un servicio confiable.<br/><br/>
<div class="titulo">Vision</div> <br/>ser la mejor empresa de seguridad a nivel naciona ofreciendo un servicio de calidad,con la tecnologia mas avanzada y los mejores profesionales todo esto con el fin de satisfacer las necesidades del cliente
a un precio mas asequible.<br/><br>
<div class="titulo">Objetivo General</div><br/> ofrecer medidad preventivas para evitar situaciones que puedan poner en peligro la seguridad de nuestros clientes.<br/>
<br/><div class="titulo">Objetivos Especificos</div><br/>
<ul class="vision">
<li><span class="icon-checkmark"></span> Generar un sentimiento de confianza y seguridad que beneficie la percepcion del cliente encuanto a la calidad del producto.</li>
<li><span class="icon-checkmark"></span> Ofrecer un servicio de calidad que cumpla con las necesidades que busca satisfacer el cliente.</li>
<li><span class="icon-checkmark"></span> Proteger confidencialidad del cliente,brindar un servicio con la mayor discrecion.</li><br/>
</ul>
</p>
</div>
<div class="imgus">
<img src='Imagenes/intala.jpg'>
</div>
</div>
</br></br>
<?php
include("piepagina.php");
pie();
?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="JS/main.js"></script>

</body>
</html>
